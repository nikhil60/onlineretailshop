Steps involved while creating a command line Online Retail Shop Application

-Identify the entities invovled in the project.
-Create a ER Diagram and identify the use cases associated with it.
-Select the technology in which we are going to build the application
-Understand what we are trying to achieve and design entites accordingly.
-Identify the locations in project which can be changed frequently.
-Use design patterns for them accordingly
-Identify the locations where exception handling needs to be done and handle them appropriately.
-Create testcases for each of the usecase which are going to be executed in project using junit.
