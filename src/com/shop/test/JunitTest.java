package com.shop.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.shop.application.CategoryFactory;
import com.shop.application.Customer;
import com.shop.application.Product;

class JunitTest {

	@Test
	void testProductCreation() {
		CategoryFactory factory = CategoryFactory.getInstance();
		try
		{
			Product product1 = new Product(1,"Bread",50,factory.getFactory(10));
			Assertions.assertTrue(product1.getProductId().equals(1));
			Assertions.assertTrue(product1.getProductName().equals("Bread"));
			Assertions.assertTrue(product1.getProductPrice().equals(50));
		}
		catch(Exception e)
		{
			e.getMessage();
		}
	}

	@Test
	void testCustomerCreation() {
		Customer customer1 = new Customer.CustomerBuilder(1, "Nikhil", "12345678").setFirstName("Nikhil").setLastName("Chavan").setEmailAddress("nikhil.chavan60@gmail.com").build();
		Assertions.assertTrue(customer1.getCustomerId().equals(1));
		Assertions.assertTrue(customer1.getUserName().equals("Nikhil"));
		Assertions.assertTrue(customer1.getPassword().equals("12345678"));
		Assertions.assertTrue(customer1.getFirstName().equals("Nikhil"));
		Assertions.assertTrue(customer1.getLastName().equals("Chavan"));
		Assertions.assertTrue(customer1.getEmailAddress().equals("nikhil.chavan60@gmail.com"));
	}
}
