package com.shop.application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class OnlineRetailShop {

	public static List<Customer> customerList = new ArrayList<Customer>();
	public static List<Product> shopproductList = new ArrayList<Product>();
	public static Map<Customer,Cart> customercartMap = new HashMap<Customer,Cart>();
	
	public static void setup()
	{
		CategoryFactory factory = CategoryFactory.getInstance();
		try
		{
			Product product1 = new Product(1,"Bread",50,factory.getFactory(10));
			Product product2 = new Product(2,"Milk",50,factory.getFactory(20));
			Product product3 = new Product(3,"Cosmetics",50,factory.getFactory(0));
			shopproductList.add(product1);
			shopproductList.add(product2);
			shopproductList.add(product3);
		}
		catch(CategoryNotFoundException e)
		{
			e.printStackTrace();
		}
		
		Customer customer1 = new Customer.CustomerBuilder(1, "Nikhil", "12345678").setFirstName("Nikhil").setLastName("Chavan").setEmailAddress("nikhil.chavan60@gmail.com").build();
		Customer customer2 = new Customer.CustomerBuilder(1, "Milind", "12345678").setFirstName("Milind").setLastName("Chavan").setEmailAddress("milind.chavan60@gmail.com").build();
		customerList.add(customer1);
		customerList.add(customer2);
	}
	
	public static Customer login(String username,String password)
	{
		for(Customer customer : customerList)
		{
			if(customer.getUserName().equals(username) && customer.getPassword().equals(password))
				return customer;
		}
		return null;
	}
	
	public static void main(String args[])
	{
		String appUser,appPass;
		Integer choice;
		setup();
		System.out.println("Enter username and password");
		Scanner sc = new Scanner(System.in);
		appUser = sc.nextLine().toString();
		appPass = sc.nextLine().toString();
		if(login(appUser,appPass)!=null)
		{
			Cart cart = new Cart();
			
			System.out.println("-------------Welcome----------------");
			System.out.println("1.View all product");
			System.out.println("2.Add product to cart");
			System.out.println("3.View product in cart");
			System.out.println("4.Generate Bill");
			System.out.println("5.Exit");
			System.out.println("Enter your choice");
			
			while(true)
			{
				choice=sc.nextInt();
				
				switch(choice)
				{
					case 1 :
							printProducts(shopproductList);
							break;
					case 2:
							System.out.flush();
							printProducts(shopproductList);
							System.out.println("Enter your choice");
							Product tempproduct = findProductById(sc.nextInt());
							System.out.println(tempproduct.getProductName());
							cart.addProduct(tempproduct);
							break;
					case 3:
							printProducts(cart.getProducts());
							break;
							
					case 4:
							customercartMap.put(login(appUser,appPass), cart);
							generateBill(login(appUser,appPass));
							break;
					default:
							System.out.println("Invalid Choice");						
							break;
				}
				if(choice>4 || choice <1)
					break;
			}
			sc.close();
		}
		else
		{
			System.out.println("-------------Invalid Login----------------");
		}
	}
	
	public static void printProducts(List<Product> productList)
	{
		for(Product product: productList)
		{
			System.out.println(product.getProductId()+". "+product.getProductName());
		}
	}
	
	public static Product findProductById(Integer productId)
	{
		for(Product product : shopproductList)
		{
			if(product.getProductId().equals(productId))
					return product;
		}
		return null;
	}
	
	public static void generateBill(Customer customer)
	{
		Cart tempCart = customercartMap.get(customer);
		System.out.println("-----------Bill------------");
		System.out.println("Customer Name : "+customer.getFirstName()+" "+customer.getLastName());
		System.out.println("Email Address : "+customer.getEmailAddress());
		System.out.println("---------------------------------------------");
		System.out.print("Products");
		System.out.print("  ");
		System.out.print("Net Amount");
		System.out.print("  ");
		System.out.println("Tax Amount");
		for(Product cartProduct:tempCart.getProducts())
		{
			System.out.print(cartProduct.getProductId()+". "+cartProduct.getProductName());
			System.out.print("  ");
			System.out.print(cartProduct.getProductPrice());
			System.out.print("  ");
			System.out.println(cartProduct.getProductPrice()*(cartProduct.getCategory().getCategoryVat()/100));
		}
		System.out.println("Total Net Amount "+tempCart.calculateTotalNetPrice());
		System.out.println("Total Vat Amount "+tempCart.calculateTotalVATPrice());
		System.out.println("Total Amount "+tempCart.calculateTotalPrice());
	}
}
