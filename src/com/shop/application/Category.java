package com.shop.application;

public abstract class Category {

	private Integer categoryId;
	private String categoryName;
	protected Integer categoryVat=0;
	
	protected Category()
	{}
	
	public Category(Integer categoryId, String categoryName) {
		super();
		this.categoryId = categoryId;
		this.categoryName = categoryName;
	}

	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Integer getCategoryVat() {
		return categoryVat;
	}
	
	public abstract Category setCategoryVat(Integer categoryVat);
}
