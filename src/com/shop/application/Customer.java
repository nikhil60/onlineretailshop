package com.shop.application;

public class Customer {
	
	private Integer customerId;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String userName;
	private String password;
	
	private Customer()
	{}
	
	private Customer(CustomerBuilder customerBuilder)
	{
		this.customerId=customerBuilder.customerId;
		this.firstName=customerBuilder.firstName;
		this.lastName=customerBuilder.lastName;
		this.emailAddress=customerBuilder.emailAddress;
		this.userName=customerBuilder.userName;
		this.password=customerBuilder.password;
	}
	
	public Integer getCustomerId() {
		return customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public static class CustomerBuilder
	{
		private Integer customerId;
		private String firstName;
		private String lastName;
		private String emailAddress;
		private String userName;
		private String password;
		
		public CustomerBuilder(Integer customerId,String userName,String password)
		{
			this.customerId=customerId;
			this.userName=userName;
			this.password=password;
		}

		public CustomerBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}



		public CustomerBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}



		public CustomerBuilder setEmailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
			return this;
		}

		public Customer build()
		{
			return new Customer(this);
		}
	}
	
}
