package com.shop.application;

public class Product {
	
	private Integer productId;
	private String productName;
	private Integer productPrice;
	private Category category;

	protected Product()
	{}

	public Product(Integer productId,String productName,Integer productPrice,Category category)
	{
		this.productId=productId;
		this.productName=productName;
		this.productPrice=productPrice;
		this.category=category;
	}
	
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Integer productPrice) {
		this.productPrice = productPrice;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
}
