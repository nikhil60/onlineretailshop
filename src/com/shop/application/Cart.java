package com.shop.application;

import java.util.ArrayList;
import java.util.List;

public class Cart {

	private List<Product> productList =new ArrayList<Product>();
	private Integer netAmount=0;
	private Integer vatAmount=0;
	private Integer totalAmount=0;
	
	public Cart()
	{}
	
	public void addProduct(Product product)
	{
		this.productList.add(product);
	}
	
	public void removeProduct(Product product)
	{
		this.productList.remove(product);
	}
	
	public List<Product> getProducts()
	{
		return this.productList;
	}
	
	public void printCartProducts()
	{
		for(Product product:productList)
		{
			System.out.println(product.toString());
		}
	}
	
	public Integer calculateTotalPrice()
	{
		for(Product product:this.productList)
		{
			this.netAmount+=product.getProductPrice();
			this.vatAmount+=(this.netAmount * (product.getCategory().getCategoryVat()/100));
			this.totalAmount=this.netAmount+this.vatAmount;
		}
		return this.totalAmount;
	}
	
	public Integer calculateTotalVATPrice()
	{
		for(Product product:this.productList)
		{
			this.vatAmount+=(this.netAmount * (product.getCategory().getCategoryVat()/100));
		}
		return this.vatAmount;
	}
	
	public Integer calculateTotalNetPrice()
	{
		for(Product product:this.productList)
		{
			this.netAmount+=product.getProductPrice();
		}
		return this.netAmount;
	}
		
}
