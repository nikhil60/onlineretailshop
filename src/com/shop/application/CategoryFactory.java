package com.shop.application;

public class CategoryFactory {

	public static CategoryFactory categoryFactory = new CategoryFactory();
	
	public Category getFactory(Integer taxPercent) throws CategoryNotFoundException
	{
		if(taxPercent==10)
		{
			return new CategoryA(1,"CategoryA").setCategoryVat(taxPercent);
		}
		else if(taxPercent==20)
		{
			return new CategoryB(1,"CategoryB").setCategoryVat(taxPercent);
		}
		else if(taxPercent==0)
		{
			return new CategoryB(1,"CategoryC").setCategoryVat(taxPercent);
		}
		else
			throw new CategoryNotFoundException("No such category found with tax percent "+ taxPercent);
	}
	
	public static CategoryFactory getInstance()
	{
		return categoryFactory;
	}
}
